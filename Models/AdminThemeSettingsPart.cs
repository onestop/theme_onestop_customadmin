﻿using Orchard.ContentManagement;
using Orchard.ContentManagement.Records;

namespace Onestop.CustomAdmin.Models {
    public class AdminThemeSettingsPart : ContentPart{
        public string LogoUrl {
            get { return this.Retrieve(x=>x.LogoUrl); }
            set { this.Store(x=>x.LogoUrl, value); }
        }

        public string Brand {
            get { return this.Retrieve(x=>x.Brand); }
            set { this.Store(x=>x.Brand,value); }
        }

        public string Welcome {
            get { return this.Retrieve(x=>x.Welcome); }
            set { this.Store(x=>x.Welcome, value); }
        }
    }

    
}