using Onestop.CustomAdmin.Models;
using Orchard;
using Orchard.ContentManagement;

namespace Onestop.CustomAdmin.Services {
    public class AdminThemeSettingsService : IAdminThemeSettingsService {
        private readonly IWorkContextAccessor _wca;

        public AdminThemeSettingsService(IWorkContextAccessor wca) {
            _wca = wca;
        }

        public AdminThemeSettingsPart GetServiceSettings() {
            return _wca.GetContext().CurrentSite.ContentItem.As<AdminThemeSettingsPart>();
        }
    }
}